import numpy as np
from scipy.optimize import fsolve
import plotly.graph_objs as go

g = 9.81         # m/s^2


class ModelA(object):


    def __init__(self, fluid, depth_charge, target_x, target_y):
        self.solutions = []
        self.fluid = fluid
        self.fluid.debug()
        self.dc = depth_charge
        self.dc.debug()
        self.x_f = float(target_x)
        self.y_f = float(target_y)
        self.V = np.pi * 4 / 3 * self.dc.r ** 3
        self.Re = self.fluid.rho * self.fluid.Uf * self.dc.r / self.fluid.mu
        self.Fg = self.dc.rho * self.V * g
        self.Fb = self.fluid.rho * self.V * g
        if self.Re == 0.0:
            self.Cd = 1e7
        elif self.Re < 1:
            self.Cd = 24 / self.Re
        elif (self.Re > 0.1) and (self.Re < 4000):
            self.Cd = 0.28 + (6 / self.Re ** .5) + 21 / self.Re
        elif (self.Re > 750) and (self.Re < 2e5):
            self.Cd = 0.445
        elif (self.Re > 1e6):
            self.Cd = (8 * 10 ** 4) / self.Re
        elif (self.Re > 2e5) and (self.Re < 1e6):
            self.Cd = 0.445

        self.Fd = self.Cd * self.fluid.rho * self.fluid.Uf**2 * np.pi * self.dc.r ** 2 / 2
        self.Fdxy = [self.Fd * self.fluid.v_x / self.fluid.Uf, self.Fd * self.fluid.v_y / self.fluid.Uf]
        self.Ftot = [self.Fdxy[0], self.Fb - self.Fg + self.Fdxy[1]]
        self.a = [self.Ftot[0] / (self.dc.r * self.V), self.Ftot[1] / (self.dc.r * self.V)]
        print(self.a)

    def solve(self):
        self.debug()

        def solve(vals):
            t, x_i = vals
            eqn1 = (self.a[0] * t**2) / 2 + x_i - self.x_f
            eqn2 = (self.a[1] * t**2) / 2 - self.y_f
            return(eqn1, eqn2)
        ans = fsolve(solve, [5, 0])
        print ('Solution', ans)
        self.t = ans[0]
        self.x_i = ans[1]
        self.y_i = 0.
        return self.t, self.x_i

    # Note that drop_position must be called first
    def show_plot(self):
        t_array = np.linspace(0, self.t, 100)
        x_array = []
        y_array = []
        for t in t_array:
            if t==0:
                x = self.x_i
                y = self.y_i
                vx = 0
                vy = 0
            else:
                vx = vx + self.a[0]*self.t/100.
                vy = vy + self.a[1]*self.t/100.
                x = x + vx*self.t/100.
                y = y + vy*self.t/100.
            x_array = np.append(x_array, x)
            y_array = np.append(y_array, y)
        self.solutions = np.append(self.solutions, {'x': x_array, 'y': y_array})
        data_list = []
        for solution in self.solutions:
            data_list = np.append(data_list, {'x': solution['x'], 'y': solution['y'], 'type': 'scatter', 'name': '{} {}'.format(self.t, self.x_i)})
        return {
            'data': data_list,
            'config':{'displaylogo': False},
            'layout': go.Layout(
                title='Depth Charge Path',
                xaxis={'title': 'Distance from ship (m)'},
                yaxis={'title': 'Distance from ship (m)'}
            )
        }

    def add_runs(self, runs):
        for run in runs:
            self.solutions = np.append(self.solutions, run)

    def debug(self):
        print("""
        Model:
            Target_x: {:.4f} m
            Target_y: {:.4f} m
            V:        {:.4f} m³
            Re:       {:.4f}
            Fg:       {:.4f} ?
            Fb:       {:.4f} ?
            Cd:       {:.4f} ?
            Fd:       {:.4f} ?
            Fdxy:     {} ?
            Ftot:     {} ?
            a:        {} ?
        """.format(self.x_f, self.y_f, self.V, self.Re, self.Fg, self.Fb, self.Cd, self.Fd, self.Fdxy, self.Ftot, self.a))




#
#     def Ftot (Fo, Fg, Fd):
#         Fdx = Fd[0]
#         Fdy = Fd[1]
#         Fx = Fdx
#         Fy = Fo- Fg + Fdy
#         Ftup= Fx, Fy
#         return Ftot
#
#
#     # In[ ]:
#
#     def acceleration (Ftot, R, rhoo):
#         Fx = Ftot[0]
#         Fy = Ftot[1]
#         V= np.pi*4/3*R**3*rhoo
#         ax = Fx/V ### needs rho V
#         ay = Fy/V
#         a = ax, ay
#         return a
