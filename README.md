# README #
Our project outline so far is to calculate and simulate the path of a depth
charge dropped into a velocity field. So far we have decided to use Dash with
Python3.

## Resources
Docs
- [Dash User Guide](https://plot.ly/dash/) - Sadly there is no ReadTheDocs page :(
- [Dash Github page](https://github.com/plotly/dash) - Looks promising

## Files
TRAJECTORY - Contains the old project we were starting with
SimpleVersion - Contains a simpler version without the graphical interface mess.

## Getting Started
This project is pretty heavy on libraries, so I would suggest using a virtual
environment if possible.

Running the following should get you all the libraries you need if you are using
pip. The contents of `requirements.txt` were all it took for me to get
`example1.py` running.
```bash
pip install -r requirements.txt
```

Once you get this running it will host an instance of your app on a local web
server. Open a web browser to `localhost:8050` to see the app in action.

## Notes
Inputs
- ρ_d - fluid Density
- U_x - fluid current
- U_y - fluid current
-
