# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 22:13:10 2015

@author: jshammon
"""
# Import Libraries
# For Python 3 and over import "tkinter" instead of "Tkinter"
import yaml
import numpy as np
from Tkinter import *
from PIL import ImageTk, Image
import matplotlib.pyplot as plt
import pandas as pd

# Import yaml ballistics file as yfile
with open('Ballistic_data.yaml') as yfile:
    yfile = yaml.load(yfile)

path = ("Bullet_details.png")


# Start GUI
class MainWindow(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.wm_title("DJX's Trajectory Calculator")
        self.img = ImageTk.PhotoImage(Image.open(path))
        self.minsize(width=100, height=300)
        self.welcome = Label(self,text="Welcome to DJX's Bullet Trajectory Calculator", justify=CENTER)
        self.explanation_1 = Label(self,text="This program will calculate the bullet drop")
        self.explanation_2 = Label(self,text="and necessary scope adjustment for a given ")
        self.explanation_3 = Label(self,text="cartridge and range.")
        self.explanation_4 = Label(self,text="Please follow the instructions to input your")
        self.explanation_5 = Label(self,text="specific data")
        self.gen_but = Button(self,text="CONTINUE",command =lambda: self.gen_but_com())
        self.picture = Label(self, image = self.img)
        self.welcome.grid(row=0,columnspan=5)
        self.explanation_1.grid(row=1, columnspan=5)
        self.explanation_2.grid(row=2, columnspan=5)
        self.explanation_3.grid(row=3, columnspan=5)
        self.explanation_4.grid(row=4, columnspan=5)
        self.explanation_5.grid(row=5, columnspan=5)
        self.gen_but.grid(row=11, column=2)
        self.picture.grid(row=17, columnspan=5)
    
        
        #Continue Button Command function 
    def gen_but_com(self):
        self.welcome.grid_forget()         
        self.explanation_1.grid_forget()   
        self.explanation_2.grid_forget()   
        self.explanation_3.grid_forget()   
        self.explanation_4.grid_forget()
        self.explanation_5.grid_forget()
        self.gen_but.grid_forget()
        self.gen_label = Label(self,text="CALIBER")
        self.gchoice_2 = Label(self,text="Please select your rifle calibler")
        self.g22_but = Button(self,text=".223 Remmington",command=lambda: self.g22_com())
        self.g3006_but = Button(self,text=".30 06 Springfield",command=lambda: self.g3006_com())
        self.g308_but = Button(self,text=".308 Winchester",command=lambda: self.g308_com())
        self.g50_but = Button(self,text=".50 Browning Machine Gun",command=lambda: self.g50_com())
        self.g762_but=Button(self,text="7.62x39mm",command=lambda: self.g762_com())
        self.gen_label.grid(row=0, columnspan=5)
        self.gchoice_2.grid(row=1, columnspan=5)
        self.g22_but.grid(row=2, columnspan=5)
        self.g762_but.grid(row=3, columnspan=5)
        self.g308_but.grid(row=4, columnspan=5)
        self.g3006_but.grid(row=5, columnspan=5)
        self.g50_but.grid(row=6, columnspan=5)
     
        
        #Command to restart after reset button is pushed
    def gen_but_com2(self):
        self.gen_label = Label(self,text="CALIBER")
        self.gchoice_2 = Label(self,text="Please select your rifle calibler")
        self.g22_but = Button(self,text=".223 Remmington",command=lambda: self.g22_com())
        self.g3006_but = Button(self,text=".30 06 Springfield",command=lambda: self.g3006_com())
        self.g308_but = Button(self,text=".308 Winchester",command=lambda: self.g308_com())
        self.g50_but = Button(self,text=".50 Browning Machine Gun",command=lambda: self.g50_com())
        self.g762_but=Button(self,text="7.62x39mm",command=lambda: self.g762_com())
        self.gen_label.grid(row=0, columnspan=5)
        self.gchoice_2.grid(row=1, columnspan=5)
        self.g22_but.grid(row=2, columnspan=5)
        self.g762_but.grid(row=3, columnspan=5)
        self.g308_but.grid(row=4, columnspan=5)
        self.g3006_but.grid(row=5, columnspan=5)
        self.g50_but.grid(row=6, columnspan=5)
        self.picture.grid(row=17, columnspan=5)
        
        
        
        #.223 Button Command
    def g22_com(self):
        self.gen_label.grid_forget()
        self.gchoice_2.grid_forget()
        self.g22_but.grid_forget()
        self.g3006_but.grid_forget()
        self.g308_but.grid_forget()
        self.g50_but.grid_forget()
        self.g762_but.grid_forget()
        self.AKcheck=0
        self.bulletselect223lab= Label(self,text="Please Select your Cartridge")
        self.Hornandy_but = Button(self,text="Hornandy V-MAX 55gr.",command=lambda: self.assign_bullet223(".223Hornady55",1.75,"Hornandy V-MAX 55gr."))
        self.FederalFus_but = Button(self,text="Federal Fusion 62gr.",command=lambda:self.assign_bullet223(".223Federalfus62",2.0,"Federal Fusion 62gr."))
        self.WinSupX_but = Button(self,text="Winchester Super-x 64gr.",command=lambda:self.assign_bullet223(".223Win64",2.0,"Winchester Super-x 64gr."))
        self.FederalAE_but = Button(self,text="Federal American Eagle 62gr.",command=lambda:self.assign_bullet223(".223Federal62",2.0,"Federal American Eagle 62gr."))
        self.Average223_but = Button(self,text=".223 Averaged",command=lambda:self.assign_bullet223(".223Averaged",2.0,".223 Averaged"))
        self.bulletselect223lab.grid(row=0,columnspan=6)
        self.Hornandy_but.grid(row=1,column=2)
        self.FederalFus_but.grid(row=2,column=2)
        self.WinSupX_but.grid(row=3,column=2)
        self.FederalAE_but.grid(row=4,column=2)
        self.Average223_but.grid(row=5,column=2)
        
        #762 Button Command
    def g762_com(self):
        self.gen_label.grid_forget()
        self.gchoice_2.grid_forget()
        self.g22_but.grid_forget()
        self.g3006_but.grid_forget()
        self.g308_but.grid_forget()
        self.g50_but.grid_forget()
        self.g762_but.grid_forget()
        self.AKcheck=1
        self.bulletselect223lab= Label(self,text="Please Select your Cartridge")
        self.WinDef120_but = Button(self,text="Winchester Defender 120 gr.",command=lambda:self.assign_bullet762("7.62Winchester120",3.5,"Winchester Defender 120 gr."))
        self.FedFus123_but = Button(self,text="Federal Fusion 123 gr.",command=lambda:self.assign_bullet762("7.62Remington123",3.25,"Federal Fusion 123 gr."))
        self.WinFMJ123_but = Button(self,text="Winchester Full metal jacket 123 gr.",command=lambda:self.assign_bullet762("7.62Winchester123",3.5,"Winchester Full metal jacket 123 gr."))
        self.RemSofPo125_but = Button(self,text="Remington Soft point 125 gr.",command=lambda:self.assign_bullet762("7.62Remington125",3.25,"Remington Soft point 125 gr."))
        self.Average762_but = Button(self,text="7.62x39mm Averaged",command=lambda:self.assign_bullet762("7.62Averaged",3.0,"7.62x39mm Averaged"))
        self.bulletselect223lab.grid(row=0,columnspan=6)
        self.WinDef120_but.grid(row=1,column=2)
        self.FedFus123_but.grid(row=2,column=2)
        self.WinFMJ123_but.grid(row=3,column=2)
        self.RemSofPo125_but.grid(row=4,column=2)
        self.Average762_but.grid(row=5,column=2)
        
        #30 06 Button Command
    def g3006_com(self):
        self.gen_label.grid_forget()
        self.gchoice_2.grid_forget()
        self.g22_but.grid_forget()
        self.g3006_but.grid_forget()
        self.g308_but.grid_forget()
        self.g50_but.grid_forget()
        self.g762_but.grid_forget()
        self.AKcheck=0
        self.bulletselect223lab= Label(self,text="Please Select your Cartridge")
        self.WinSupx180_but = Button(self,text="Winchester Super X 180gr.",command=lambda:self.assign_bullet3006(".3006WinX180",2.5,"Winchester Super X 180gr."))
        self.WinSupx165_but = Button(self,text="Winchester Super X 165gr.",command=lambda:self.assign_bullet3006(".3006WinX165",2.25,"Winchester Super X 165gr."))
        self.FedFus180_but = Button(self,text="Federal Fusion 180gr.",command=lambda:self.assign_bullet3006(".3006Federal180",2.25,"Federal Fusion 180gr."))
        self.RemCopperSol_but = Button(self,text="Remington Copper Solid 165gr.",command=lambda:self.assign_bullet3006(".3006Remington165",2.25,"Remington Copper Solid 165gr."))
        self.Average3006_but = Button(self,text="30 06 Averaged",command=lambda:self.assign_bullet3006(".3006Averaged",2.25,"30 06 Averaged"))
        self.bulletselect223lab.grid(row=0,columnspan=6)
        self.bulletselect223lab.grid(row=0,columnspan=6)
        self.WinSupx180_but.grid(row=1,column=2)
        self.WinSupx165_but.grid(row=2,column=2)
        self.FedFus180_but.grid(row=3,column=2)
        self.RemCopperSol_but.grid(row=4,column=2)
        self.Average3006_but.grid(row=5,column=2)
        
        #.308 Button Command
    def g308_com(self):
        self.gen_label.grid_forget()
        self.gchoice_2.grid_forget()
        self.g22_but.grid_forget()
        self.g3006_but.grid_forget()
        self.g308_but.grid_forget()
        self.g50_but.grid_forget()
        self.g762_but.grid_forget()
        self.AKcheck=0
        self.bulletselect223lab= Label(self,text="Please Select your Cartridge")
        self.FedPowSho150_but = Button(self,text="Federal Power-Shok 150 gr.",command=lambda:self.assign_bullet308(".308Federal150",2.25,"Federal Power-Shok 150 gr."))
        self.FedPowSho180_but = Button(self,text="Federal Power-Shok 180 gr.",command=lambda:self.assign_bullet308(".308Federal180",2.75,"Federal Power-Shok 180 gr."))
        self.WinFMJ147_but = Button(self,text="Winchester Full metal jacket 147 gr.",command=lambda:self.assign_bullet308(".308Winchester147",2.25,"Winchester Full metal jacket 147 gr."))
        self.RemPreAccu165_but = Button(self,text="Remington Premier Accutip 165 gr.",command=lambda:self.assign_bullet308(".308Remington165",2.5,"Remington Premier Accutip 165 gr."))
        self.Average308_but = Button(self,text=".308 Averaged",command=lambda:self.assign_bullet308(".308Averaged",2.5,".308 Averaged"))
        self.bulletselect223lab.grid(row=0,columnspan=6)
        self.FedPowSho150_but.grid(row=1,column=2)
        self.FedPowSho180_but.grid(row=2,column=2)
        self.WinFMJ147_but.grid(row=3,column=2)
        self.RemPreAccu165_but.grid(row=4,column=2)
        self.Average308_but.grid(row=5,column=2)
        
        #.50 cal Button Command
    def g50_com(self):
        self.gen_label.grid_forget()
        self.gchoice_2.grid_forget()
        self.g22_but.grid_forget()
        self.g3006_but.grid_forget()
        self.g308_but.grid_forget()
        self.g50_but.grid_forget()
        self.g762_but.grid_forget()
        self.AKcheck=0
        self.bulletselect223lab= Label(self,text="Please Select your Barrel Length")
        self.Bar29in_but = Button(self,text="Barrett 29” barrel",command=lambda:self.assign_bullet50(".50Barrett29",2.25,"Barrett 29” barrel"))
        self.Bar20in_but = Button(self,text="Barrett 20” barrel",command=lambda:self.assign_bullet50(".50Barrett20",2.75,"Barrett 20” barrel"))
        self.Average50_but = Button(self,text=".50 Averaged",command=lambda:self.assign_bullet50(".50Averaged",2.5,"Barrett 50 Cal Average"))
        self.bulletselect223lab.grid(row=0,columnspan=6)
        self.Bar29in_but.grid(row=1,column=2)
        self.Bar20in_but.grid(row=2,column=2)
        self.Average50_but.grid(row=3,column=2)
    
    
        #Command executed when .223 cartridge is selected
    def assign_bullet223(self,bullet,MOA100,name):
        self.bullet = bullet
        self.bulletselect223lab.grid_forget()
        self.Hornandy_but.grid_forget()
        self.FederalFus_but.grid_forget()
        self.WinSupX_but.grid_forget()
        self.FederalAE_but.grid_forget()
        self.Average223_but.grid_forget()
        self.range_label = Label(self,text="Please Enter your range (1 to 500 yards):")
        self.getyards = Entry(self)
        self.calculate_but = Button(self,text="Calculate",command=lambda: self.calculate_com())
        self.range_label.grid(row=0,columnspan=5)
        self.getyards.grid(row=1,columnspan=5)
        self.calculate_but.grid(row=2,column=2)
        self.MOA100=MOA100
        self.name=name
        
        #Command executed when 30 06 cartridge is selected
    def assign_bullet3006(self,bullet,MOA100,name):
        self.bullet = bullet
        self.bulletselect223lab.grid_forget()
        self.WinSupx180_but.grid_forget()
        self.WinSupx165_but.grid_forget()
        self.FedFus180_but.grid_forget()
        self.RemCopperSol_but.grid_forget()
        self.Average3006_but.grid_forget()
        self.range_label = Label(self,text="Please Enter your range (1 to 500 yards):")
        self.getyards = Entry(self)
        self.calculate_but = Button(self,text="Calculate",command=lambda: self.calculate_com())
        self.range_label.grid(row=0,columnspan=5)
        self.getyards.grid(row=1,columnspan=5)
        self.calculate_but.grid(row=2,column=2)
        self.MOA100=MOA100
        self.name=name
        
        #Command excecuted when .308 cartridge is selected
    def assign_bullet308(self,bullet,MOA100,name):
        self.bullet = bullet
        self.bulletselect223lab.grid_forget()
        self.FedPowSho150_but.grid_forget()
        self.FedPowSho180_but.grid_forget()
        self.WinFMJ147_but.grid_forget()
        self.RemPreAccu165_but.grid_forget()
        self.Average308_but.grid_forget()
        self.range_label = Label(self,text="Please Enter your range (1 to 500 yards):")
        self.getyards = Entry(self)
        self.calculate_but = Button(self,text="Calculate",command=lambda: self.calculate_com())
        self.range_label.grid(row=0,columnspan=5)
        self.getyards.grid(row=1,columnspan=5)
        self.calculate_but.grid(row=2,column=2)
        self.MOA100=MOA100
        self.name=name
        
        #Command excecuted when 762 cartridge is selected        
    def assign_bullet762(self,bullet,MOA100,name):
        self.bullet = bullet
        self.bulletselect223lab.grid_forget()
        self.WinDef120_but.grid_forget()
        self.FedFus123_but.grid_forget()
        self.WinFMJ123_but.grid_forget()
        self.RemSofPo125_but.grid_forget()
        self.Average762_but.grid_forget()
        self.range_label = Label(self,text="Please Enter your range (1 to 300 yards):")
        self.getyards = Entry(self)
        self.calculate_but = Button(self,text="Calculate",command=lambda: self.calculate_com())
        self.range_label.grid(row=0,columnspan=5)
        self.getyards.grid(row=1,columnspan=5)
        self.calculate_but.grid(row=2,column=2)
        self.MOA100=MOA100
        self.name=name
    
        #Command executed when 50 cal cartridge is selected
    def assign_bullet50(self,bullet,MOA100,name):
        self.bullet = bullet
        self.bulletselect223lab.grid_forget()
        self.Bar29in_but.grid_forget()
        self.Bar20in_but.grid_forget()
        self.Average50_but.grid_forget()
        self.range_label = Label(self,text="Please Enter your range (1 to 500 yards):")
        self.getyards = Entry(self)
        self.calculate_but = Button(self,text="Calculate",command=lambda: self.calculate_com())
        self.range_label.grid(row=0,columnspan=5)
        self.getyards.grid(row=1,columnspan=5)
        self.calculate_but.grid(row=2,column=2)
        self.MOA100=MOA100
        self.name=name
        
        #Command executed if an invalid range input is detected
    def error_yards(self):
        self.range_label.grid_forget()
        self.getyards.grid_forget()
        self.calculate_but.grid_forget()
        self.range_label2 = Label(self,text="Please Enter a Valid Range (1 to 500 yards):")
        self.calculate_but2 = Button(self,text="Calculate",command=lambda: self.calculate2_com())
        self.range_label2.grid(row=0,columnspan=5)
        self.getyards.grid(row=1,columnspan=5)
        self.calculate_but2.grid(row=2,column=2)
        
        #Command executed if an invalid range input is detected for AK bullets
    def error_yardsAK(self):
        self.range_label.grid_forget()
        self.getyards.grid_forget()
        self.calculate_but.grid_forget()
        self.range_label2 = Label(self,text="Please Enter a Valid Range (1 to 300 yards):")
        self.calculate_but2 = Button(self,text="Calculate",command=lambda: self.calculate2_com())
        self.range_label2.grid(row=0,columnspan=5)
        self.getyards.grid(row=1,columnspan=5)
        self.calculate_but2.grid(row=2,column=2)
        
        
        #Verifies range input is a number 
    def verify_numbers(self):      
        try:
            self.yards = float(self.yards)
        except ValueError:
            self.error_yards()
        else:
            if self.AKcheck==0:
                self.verify_range()
            else:
                self.verify_rangeAK()
            
        #Verifies range input is not a negative number   
    def verify_range2(self):
        if self.yards > 0.:
            self.Final()
        else:
            self.error_yards()
    
        #Verifies range input is under 500 yards
    def verify_range(self):      
        if self.yards <= 500.0:
            self.verify_range2()
        else: 
            self.error_yards()
         #Verifies range input is under 300 yards for AK bullets   
    def verify_rangeAK(self):
        if self.yards <= 300.0:
            self.verify_range2()
        else: 
            self.error_yardsAK()
        
            
    
        
        
        
        
        
        #Command excecuted when Calculate Button is pressed    
    def calculate_com(self):
        self.range_label.grid_forget()
        self.getyards.grid_forget()
        self.calculate_but.grid_forget()
        self.picture.grid_forget()
        self.yards=self.getyards.get()
        self.verify_numbers()
        
        #Command executed when Calculate Button is pressed after an invalid range entry
    def calculate2_com(self):
        self.range_label2.grid_forget()
        self.getyards.grid_forget()
        self.calculate_but2.grid_forget()
        self.picture.grid_forget()
        self.yards=self.getyards.get()
        self.verify_numbers()
        
        
       
            
        
    
    # Define functions for approximating the acceleration
    def find_a_avg(self,x):
            self.a_avg = (self.find_a(0)+self.find_a(x))/2
            return self.a_avg

    def find_a(self,x):
            self.a = self.a_coef[0]*x + self.a_coef[1]
            self.a = self.a*0.3048                # meters/s^2
            return self.a    
    
    
        #Command executed when reset button is pressed 
    def reset_com(self):
        self.finlab1.grid_forget()
        self.finlab2.grid_forget()
        self.finlab3.grid_forget()
        self.finlab4.grid_forget()
        self.finlab5.grid_forget()
        self.finlab6.grid_forget()
        self.finlab7.grid_forget()
        self.finlab8.grid_forget()
        self.finlab9.grid_forget()
        self.finlab12.grid_forget()
        self.finlab13.grid_forget()
        self.finlab14.grid_forget()
        self.finlab15.grid_forget()
        self.finlab16.grid_forget()
        self.finlab17.grid_forget()
        self.finlab18.grid_forget()    
        self.reset_but.grid_forget()
        self.gen_but_com2()
        
    def find_y(self,t):
        self.Y = self.V_y*t - 4.9*t**2
        return self.Y
        
        
        #Command executed after verification functions when Calculate button is pressed
    def Final(self):
    
        # Set variables to call for bullet ballistics    
        self.velocity = "V_initial"
        self.drag_coef = "k"
        self.caliber = "caliber"
        self.accel = "a"
        self.weight = "weight"
        self.drop = "drop"


        # Gather bullet ballistics data from yfile
        self.fps = float(yfile[self.bullet][self.velocity])
        self.k = float(yfile[self.bullet][self.drag_coef])
        self.cal = yfile[self.bullet][self.caliber]            # inches
        self.a_coef = yfile[self.bullet][self.accel]
        self.weight = float(yfile[self.bullet][self.weight])
        self.drop= float(yfile[self.bullet][self.drop])





        # Convert ballistics data to metric units
        self.distance = self.yards/1.0936      # yards to meters
        self.mps = self.fps / 3.28084             # fps to mps



        # solve for distance by angle
        self.err = 100                     # presets error in meters
        self.X_i = 0
        self.angle = 0.0001         # degrees
        self.Y_array=np.empty(0)
        if self.yards < 300:        #for 300 yards and below the accuracy is within 2 yards of inputed distance
            self.errmax = 2
        elif self.yards < 450:      #for 450 yards and below the accuracy is within 5 yards of inputed distance
            self.errmax=5
        else:
            self.errmax = 10        #for 450 yards and above the accuracy is within 10 yards of inputed distance
            
        while self.err > self.errmax:               # Meters of error
            self.X_i = 0
            if (self.angle > 1):    #10 degrees in radians
                print ("Solver Error!")
                break
            # break into vectors
            self.X_array=np.empty(0)  
            self.V_y = self.mps * np.sin(self.angle)
            self.V_x = self.mps * np.cos(self.angle)
            self.velocity_dynamic=self.V_x
            self.time = self.V_y / 4.9        # Solves for the final time
            self.time_array = np.linspace(0, self.time, num = 100)
            self.dt = self.time/100
            # Calculate distance given velocity and angle
            for i in self.time_array:
                self.dX = self.velocity_dynamic*self.dt + .5*self.find_a(self.X_i) * self.dt**2
                self.X_f = self.X_i+self.dX
                self.X_array=np.append(self.X_array, self.X_f)
                self.velocity_dynamic = self.velocity_dynamic + self.dt*self.find_a(self.X_f)
                self.X_i = self.X_f
            # Compare distances and find the abs err
            self.err = abs(self.distance - self.X_f)
            self.angle = self.angle + 0.000001
        # End While loop    
        
     
        
        
        
        
        #Graphs the bullet trajectory and saves it as a png file
        self.Y_array=self.find_y(self.time_array)
        
        self.X_array_yards=self.X_array*1.09361
        self.Y_array_yards=self.Y_array*1.09361
        
        self.fig = plt.figure(1)
        plt.plot(self.X_array_yards,self.Y_array_yards)
        plt.xlabel("Distance (yards)")
        plt.ylabel("Height (yards)")
        self.fig.savefig("trajectory.png")
       
        
        
        #Finds final bullet drop 
        self.y_f = -4.9*(self.time_array[99]**2)
        self.y_f=abs(self.y_f)

        
        
        self.y_finches= self.y_f*39.370 #convert bullet drop to inches
        self.MOA = self.yards/100.00    #find MOA at range
        self.MOA_Adjust = (self.y_finches/self.MOA)-self.MOA100 #find MOA adjustment needed
        
        
        #GUI OUTPUT
        self.finlab1=Label(self,text=("Bullet Info:"))
        self.finlab2=Label(self,text=("Cartridge: "+self.name))
        self.finlab3=Label(self,text=("Caliber: " +self.cal))
        self.finlab4=Label(self,text=("Ballistic coeff: {:.3f}".format(self.k)))
        self.finlab5=Label(self,text=("Muzzle Velocity: {:.0f} ft/s".format(self.fps)))
        self.finlab6=Label(self,text=("Bullet Weight: "+str(self.weight)+"grains"))
        self.finlab7=Label(self,text="---------------------------------------------")
        self.finlab8=Label(self,text="Calculation Data:")
        self.finlab9=Label(self,text=("Angle of drop {:.2e} degrees".format(self.angle*180.0/np.pi)))
        self.finlab12=Label(self,text=("Time: {:.2f} seconds".format(self.time_array[99])))
        self.finlab13=Label(self,text=("Bullet Drop: {:.2f} inches at {:.2f} yards".format(self.y_finches, self.yards)))
        self.finlab14=Label(self,text=("MOA Adjustment(scope sighted for 100 yards): {:.2f} MOA".format(self.MOA_Adjust)))
        self.finlab15=Label(self,text="---------------------------------------------")
        self.finlab16=Label(self,text="A Graph of the trajectory has been saved to working directory as 'trajectory.png'")
        self.finlab17=Label(self,text="A CSV file for data analysis of trajectory has been saved as 'trajectory.csv'")
        self.finlab18=Label(self,text="In CSV File output 0 = Time, 1 = X values, 2 = Y values")
        self.reset_but=Button(self,text=("RESET"),command=lambda:self.reset_com())
        self.finlab1.grid(row=0,columnspan=5)
        self.finlab2.grid(row=1,columnspan=5)
        self.finlab3.grid(row=2,columnspan=5)
        self.finlab4.grid(row=3,columnspan=5)
        self.finlab5.grid(row=4,columnspan=5)
        self.finlab6.grid(row=5,columnspan=5)
        self.finlab7.grid(row=6,columnspan=5)
        self.finlab8.grid(row=7,columnspan=5)
        self.finlab9.grid(row=8,columnspan=5)
        self.finlab12.grid(row=9,columnspan=5)
        self.finlab13.grid(row=10,columnspan=5)
        self.finlab14.grid(row=11,columnspan=5)
        self.finlab15.grid(row=12,columnspan=5)
        self.finlab16.grid(row=13,columnspan=5)
        self.finlab17.grid(row=14,columnspan=5)
        self.finlab18.grid(row=15,columnspan=5)    
        self.reset_but.grid(row=16,column=2)
        
        
        
        #Outputs x and y values of bullet trajectory to csv file
        self.graphoutput = [self.time_array, self.X_array_yards, self.Y_array_yards]
        self.graphoutput = np.transpose(self.graphoutput)
        self.graphoutputf = pd.DataFrame(self.graphoutput)
        self.graphoutputf.to_csv("trajectory.csv")
     
        
        
        
Main = MainWindow()
Main.mainloop()

