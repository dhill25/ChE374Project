# -*- coding: utf-8 -*-
from __future__ import division

"""
Created on Fri Nov 13 09:04:08 2015

Python Project 2

@author: Team Pythonidae

Members:
Jared Hammon
Xirong Zhang
Daniel Hill
"""

"""
Pseudo Code:
Import libraries and yaml file

Start GUI
Get user input from GUI:
    Choose cartridge type
            .308
            .30 06
            .22
            .223
            .50        
    Choose a specific cartridge from list or choose to enter bullet grain manually
    Set target distance
        0-1000 yds

Gather cartridge ballistics data from yaml file  
  
Perform unit conversions to metric
    
Use ballistics data to calculate the necessary shooting angle
        Break velocity into x and y components
        Solve for V_y=0
        Find time
        Find Angle
        
Graph the shooting trajectory

Convert angle into click calibrations for a standard scope

Output to GUI:
    shooting angle
    number of clicks on the scope
    Graph of trajectory
    
"""
# Other imports needed?
import yaml
import matplotlib.pyplot as plt
import numpy as np

# import yaml ballistics file as yfile
with open('Ballistic_data.yaml') as yfile:
    yfile = yaml.load(yfile)
    
# Start GUI    
#   take arguments of cartridge type and distance

bullet = "WinX180"
yards = 500                  # yards

# Set variables to call for bullet ballistics    
velocity = "V_initial"
drag_coef = "k"
caliber = "caliber"
accel = "a_new"
weight = "weight"

# Gather bullet ballistics data from yfile
fps = float(yfile[bullet][velocity])
k = float(yfile[bullet][drag_coef])
cal = yfile[bullet][caliber]            # inches
a_coef = yfile[bullet][accel]
weight = float(yfile[bullet][weight])

def find_a_avg(x):
    a_avg = (find_a(0)+find_a(x))/2
    return a_avg


def find_a(x):
    a = a_coef[0]*x + a_coef[1]
    a = a*0.3048                # meters/s^2
    return a
    
print (find_a(0))
print (find_a(1000))
# Convert ballistics data to metric units
distance = yards/1.0936      # yards to meters
mps = fps / 3.28084             # fps to mps



# Use ballistic data to find angle
# x_f = -1/2*a*t**2 + v_i*t


a=find_a_avg(distance)
b=mps
c=-distance


# NEW METHOD
# works around sqrt(-) with good accuracy
# Does not account for where the gun is already sighted for. Take that into account in the clicks?
error = 100
t = 0
while (error > 1):
    if (t > 1.5):
        print ("Solver error!")
        break
    x_f = 0.5*a*t**2 + mps*t
    error = abs(distance - x_f)
    t = t + .00001

# OLD METHOD
#t = (-b-np.sqrt(b**2-4*a*c))/2*a
#t = (-mps-np.sqrt(mps**2-4*(a/2)*distance))/2*(a/2)



y_f = -4.9*t**2
angle = np.arctan(y_f/distance)


angle = angle*(np.pi/180)

print ("Angle: " + str(angle)+" degrees")
print ("Given distance: "+str(distance)+" meters")
print ("Calculated distance: "+str(x_f)+" meters")
print ("Time: "+ str(t)+ " seconds")
print ("Bullet Drop: " + str(y_f)+" meters")
#print t

# Use angle to find scope setting

clicks = 4                      # Clicks on a standard scope


# Output angle, clicks, and trajectory chart


"""
# Make trajectory chart
def height(x):
    pass
x = np.linspace(0,yards)
y = height(x)
#max_height = np.max(y)

plt.figure(1)
plt.plot(x,y)
plt.xlabel("distance (yds)")
plt.ylabel("height (yds)")
plt.show()
"""

"""
Notes:
because we are only using the first part of the trajectory we can approximate the drag as being a linear regression
it could be far easier to do a regression of the bullets drop at different angles
This would bypass all of the difficult physics while providing an even more accurate result
We need to take into account where the gun is sighted for.


Need to change acceleration coefficients to a quadratic regression over time
"""


