# -*- coding: utf-8 -*-
import dash
from dash.dependencies import Input, Output, State
import json

from fluid import Fluid
from model_a import ModelA
from model_b import ModelB
from depth_charge import Depth_Charge
from layout import layout

app = dash.Dash()


# HTML Layout stored in layout.py
app.layout = layout
css_url = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
jquery = "https://code.jquery.com/jquery-3.2.1.slim.min.js"
js_url = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
bootstrap_js = "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
app.css.append_css({"external_url": css_url})
app.css.append_css({"external_url": "https://codepen.io/dhill2522/pen/xPeyNV.css"})
app.scripts.append_script({"external_url": jquery})
app.scripts.append_script({"external_url": js_url})
app.scripts.append_script({"external_url": bootstrap_js})

def validate(value, min_val, max_val, prop):
    if (float(value) < min_val) or (float(value) > max_val):
        print("Error!", prop, "outside of range", min_val, '-', max_val)


@app.callback(
    Output('fluid_density', 'value'),
    [
        Input('fluid_velocity_x', 'value')
    ]
)
def density_validator(value):
    if int(value) < 1100:
        return 1100
    elif int(value) > 10000:
        return 10000

@app.callback(
    Output('solution-graph', 'figure'),
    [
        Input('start', 'n_clicks'),
    ],
    state=[
        State('fluid_density', 'value'),
        State('fluid_velocity_x', 'value'),
        State('fluid_velocity_y', 'value'),
        State('fluid_viscosity', 'value'),
        State('dc_density', 'value'),
        State('dc_radius', 'value'),
        State('target_x', 'value'),
        State('target_y', 'value'),
        State('methodRadio', 'value')
    ]
)
def start(start, fluid_density, fluid_velocity_x, fluid_velocity_y,
            fluid_viscosity, dc_density, dc_radius, target_x, target_y, method):
    print("Run Started with method ", method)
    validate(fluid_density, 1100, 10000, 'Fluid density')
    validate(fluid_velocity_x, -1, 1, 'Velocity in x')
    validate(fluid_velocity_y, -0.1, 0.1, 'Velocity in y')
    validate(fluid_viscosity, 0, .01, 'Viscosity')
    validate(dc_density, 5000, 10000, 'Object density')
    validate(dc_radius, 0.01, 1, 'Object radius')
    validate(target_x, -1000, 1000, 'target x coord.')
    validate(target_y, -1000, 0, 'target y coord.')
    fluid_density = float(fluid_density)
    fluid_velocity_x = float(fluid_velocity_x)
    fluid_velocity_y = float(fluid_velocity_y)
    fluid_viscosity = float(fluid_viscosity)
    dc_density = float(dc_density)
    dc_radius = float(dc_radius)
    target_x = float(target_x)
    target_y = float(target_y)
    f = Fluid(fluid_density, fluid_velocity_x, fluid_velocity_y, fluid_viscosity)
    dc = Depth_Charge(dc_density, dc_radius)
    if method == 'A':
        model = ModelA(f, dc, target_x, target_y)
    else:
        model = ModelB(f, dc, target_x, target_y)
    solution = model.solve()
    plot_data = model.show_plot()
    return(plot_data)

@app.callback(
    Output('time', 'children'),
    [Input('solution-graph', 'figure')]
)
def update_time(figure):
    t = figure['data']
    print(t[0]['name'])
    t = float(t[0]['name'].split(' ')[0])
    print("time to target:", t)
    return '{:.2f} s'.format(t)

@app.callback(
    Output('placement', 'children'),
    [Input('solution-graph', 'figure')]
)
def update_time(figure):
    t = figure['data']
    t = float(t[0]['name'].split(' ')[1])
    print("charge drop placement:", t)
    return '{:.2f} m'.format(t)

if __name__ == '__main__':
    app.run_server(debug=True)
