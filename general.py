import numpy as np
from fluid import Fluid
from depth_charge import Depth_Charge

class General(object):
    def __init__(self, fluid, field, dc: Depth_Charge):
        self.fluid = Fluid(fluid)
        self.field = field
        # self.time = time
        # self.time_array = np.linspace(0, time, 100)
        self.F_b = 20
        self.F_d = 20
        self.F_g = 9.8 * dc.m
        self.F_total = self.F_g - self.F_b - self.F_d
        self.a = self.F_total/dc.m
