import numpy as np
# Modeling the depth charge as a sphere of steel with the properties below
class Depth_Charge(object):
    def __init__(self, dc_density, dc_radius):
        self.rho = dc_density
        self.r = dc_radius

    def debug(self):
        print("""
        Depth Charge Properties:
            ρ:  {:.4f} kg/m³
            r:  {:.4f} m
        """.format(self.rho, self.r))
