import numpy as np
from scipy.optimize import fsolve
import plotly.graph_objs as go

g = 9.81         # m/s^2


class ModelB(object):


    def __init__(self, fluid, depth_charge, target_x, target_y):
        self.solutions = []
        self.fluid = fluid
        self.fluid.debug()
        self.dc = depth_charge
        self.dc.debug()
        self.x_f = float(target_x)
        self.y_f = float(target_y)
        self.V = np.pi * 4. / 3. * self.dc.r ** 3
        self.Fg = self.dc.rho * self.V * g
        self.Fb = self.fluid.rho * self.V * g

    def Re(self,U):
        return self.fluid.rho * U * self.dc.r / self.fluid.mu

    def Cd(self,Re):
        if Re == 0.0:
            Cd = 1.e7
        elif Re < 1.:
            Cd = 24. / Re
        elif (Re > 0.1) and (Re < 4000.):
            Cd = 0.28 + (6. / Re ** .5) + 21. / Re
        elif (Re > 750.) and (Re < 2.e5):
            Cd = 0.445
        elif (Re > 1.e6):
            Cd = (8. * 10 ** 4) / Re
        elif (Re > 2.e5) and (Re < 1.e6):
            Cd = 0.445
        return Cd

    def Fd(self,U,Cd):
        return Cd*self.fluid.rho*np.power(U,2)*np.pi*np.power(self.dc.r,2)/2.0

    def solve(self):
        #self.debug()

        def ysolve(Uy_rel_guess):
            return self.Fb - self.Fg + self.Fd(Uy_rel_guess,self.Cd(self.Re(Uy_rel_guess)))[0]
        Uy_rel = fsolve(ysolve, self.fluid.v_y)
        self.vy = self.fluid.v_y - Uy_rel
        tf = self.y_f / self.vy

        # debug
        print("self.Fb", self.Fb)
        print("self.Fg", self.Fg)
        print(self.Fd(Uy_rel,self.Cd(self.Re(Uy_rel))))
        print("Uy_rel", Uy_rel)
        print("tf", tf)

        x_buffer = []
        for t in np.linspace(0,tf,100):
            if t==0:
                x = float(0.)
                vx = 0.
                Ux_rel = self.fluid.v_x
            else:
                x = float(x + tf/100. * vx)
            x_buffer = np.append(x_buffer,x)

            # for next iteration
            Fdx = self.Fd(Ux_rel,self.Cd(self.Re(Ux_rel)))
            ax = 3.*Fdx / ( 4.*np.power(self.dc.r,3)*np.pi*self.dc.rho )
            vx += tf/100. * ax
            Ux_rel = self.fluid.v_x - vx

        print(x_buffer)
        x_f = x_buffer[-1]
        x_buffer = [x-(x_f-self.x_f) for x in x_buffer]
        self.x_array = x_buffer
        self.tf = tf

        print(self.vy)

        return tf, x_buffer[0]

    # Note that drop_position must be called first
    def show_plot(self):
        t_array = np.linspace(0, self.tf, 100)
        x_array = self.x_array
        y_array = []
        for t in t_array:
            y_array = np.append(y_array, t*self.vy)
        self.solutions = np.append(self.solutions, {'x': x_array, 'y': y_array})
        data_list = []
        for solution in self.solutions:
            data_list = np.append(data_list, {'x': solution['x'], 'y': solution['y'], 'type': 'scatter', 'name': '{} {}'.format(self.tf[0], self.x_array[0])})
        return {
            'data': data_list,
            'config':{'displaylogo': False},
            'layout': go.Layout(
                title='Depth Charge Path',
                xaxis={'title': 'Distance from ship (m)'},
                yaxis={'title': 'Distance from ship (m)'}
            )
        }

    def add_runs(self, runs):
        for run in runs:
            self.solutions = np.append(self.solutions, run)

    def debug(self):
        print("""
        Model:
            Target_x: {:.4f} m
            Target_y: {:.4f} m
            V:        {:.4f} m³
            Re:       {:.4f}
            Fg:       {:.4f} ?
            Fb:       {:.4f} ?
        """.format(self.x_f, self.y_f, self.V, self.Re, self.Fg, self.Fb))




#
#     def Ftot (Fo, Fg, Fd):
#         Fdx = Fd[0]
#         Fdy = Fd[1]
#         Fx = Fdx
#         Fy = Fo- Fg + Fdy
#         Ftup= Fx, Fy
#         return Ftot
#
#
#     # In[ ]:
#
#     def acceleration (Ftot, R, rhoo):
#         Fx = Ftot[0]
#         Fy = Ftot[1]
#         V= np.pi*4/3*R**3*rhoo
#         ax = Fx/V ### needs rho V
#         ay = Fy/V
#         a = ax, ay
#         return a
