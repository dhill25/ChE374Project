import numpy as np

class Fluid(object):
    def __init__(self, fluid_density, fluid_velocity_x, fluid_velocity_y, fluid_viscosity):
        self.rho = fluid_density
        self.v_x = fluid_velocity_x
        self.v_y = fluid_velocity_y
        self.mu = fluid_viscosity
        self.Uf = np.sqrt(self.v_x**2 + self.v_y**2)

    def debug(self):
        print("""
        Fluid Properties:
            ρ:   {:.4f} kg/m³
            vᵧ:  {:.4f} m/s
            vₓ:  {:.4f} m/s
            μ:   {:.4f} Pa s
            U:   {:.4f} m/s
        """.format(self.rho, self.v_x, self.v_y, self.mu, self.Uf))
