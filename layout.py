# -*- coding: utf-8 -*-
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html

layout = html.Div(children=[
    html.H1(children='Depth Charge in a 2D Vector Field', style={'text-align': 'center'}),
        html.Div(
            className='col-sm-4',
            children=[
                html.H4('Fluid Properties'),
                html.Label('Density (kg/m³):'),
                dcc.Input(
                    id='fluid_density',
                    type='number',
                    value='4000',
                    placeholder='1100-10000'
                ),
                html.P(),
                html.Label('Velocity in x (m/s):'),
                dcc.Input(
                    id='fluid_velocity_x',
                    type='number',
                    value='1',
                    placeholder='-1.000 - 1.000'
                ),
                html.P(),
                html.Label('Velocity in y (m/s):'),
                dcc.Input(
                    id='fluid_velocity_y',
                    type='number',
                    value='1',
                    placeholder='-1.000 - 1.000'
                ),
                html.P(),
                html.Label('Viscosity (Pa s):'),
                dcc.Input(
                    id='fluid_viscosity',
                    type='number',
                    value='10e-3',
                    placeholder='reasonable values only'
                ),
                html.Hr(),
                html.H4('Depth Charge Properties'),
                html.Label('Density (kg/m³):'),
                dcc.Input(
                    id='dc_density',
                    type='number',
                    value='8050',
                    placeholder='reasonable values only'
                ),
                html.P(),
                html.Label('Radius (m):'),
                dcc.Input(
                    id='dc_radius',
                    type='number',
                    value='0.25',
                    placeholder='reasonable values only'
                ),
                html.Hr(),
                html.H4('Choose Method'),
                dcc.RadioItems(
                    id='methodRadio',
                    options=[
                        {
                            'label': 'Method A',
                            'value': 'A',
                        },
                        {
                            'label': 'Method B',
                            'value': 'B'
                        }
                    ],
                    value='B',
                    labelClassName='radioLabel'
                ),
                html.Hr(),
                html.H4('Target Location'),
                html.P('Coordinates should be given relative to the position of the ship.'),
                html.Label('X-coordinate (m):'),
                dcc.Input(
                    id='target_x',
                    type='number',
                    value='0',
                    placeholder='reasonable values only'
                ),
                html.P(),
                html.Label('Y-coordinate (m):'),
                dcc.Input(
                    id='target_y',
                    type='number',
                    value='-100',
                    placeholder='reasonable values only'
                ),
                html.Hr(),
                html.Button(id='start', children='start', className='btn btn-primary')
        ]),
        html.Div(
            className='col-sm-8',
            children=[
                dcc.Graph(
                    id='solution-graph',
                    figure={
                    },
                ),
                html.Hr(),
                html.H4('Calculation Outputs:'),
                html.H6('Time to target (s)'),
                html.P(id='time'),
                html.H6('Drop placement (m)'),
                html.P(id='placement'),
                html.P(id='log', style={'color': 'red'}),
            ]
        )
    ])
